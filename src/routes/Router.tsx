import React from "react";
import {BrowserRouter, Route, Switch, Redirect} from 'react-router-dom';
import UserList from "../components/userList/UserList";

const Router = () => {

    return (
            <BrowserRouter>
                    <Switch>
                        <Route exact path={'/'}>
                            <Redirect to={'/userList'}/>
                        </Route>
                        <Route path={'/userList'}>
                            <UserList/>
                        </Route>
                    </Switch>
            </BrowserRouter>
    )

}

export default Router;