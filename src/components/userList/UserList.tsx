import React, {useEffect, useState} from "react";
import {User} from "../../entities/User";
import './UserList.style.scss';
import Table from "../table/Table";
import {colorChecker, months} from "./months";

export interface MonthsInfo {
    name: string;
    color: string;
    count: number;
}

const UserList: React.FC = () => {

    const [usersData, setUsersData] = useState<User[]>([]);
    const [isFetching, setIsFetching] = useState<boolean>(false);
    const [monthsInfo, setMonthsInfo] = useState<MonthsInfo[]>([]);

    const createMonthInfo = () => {
        const info = months.map(item => {
            let count = 0;
            usersData.forEach(user => {
                if((new Date(user.dob).getMonth() + 1) === item.count) {
                    count += 1;
                }
            })
            return  ({
                name: item.name,
                color: colorChecker(count),
                count: item.count
            })
        })
        setMonthsInfo(info);
    }

    useEffect(() => {
        setIsFetching(true);
        fetch('https://yalantis-react-school-api.yalantis.com/api/task0/users')
            .then(res => res.json())
            .then(data => {
                setIsFetching(false);
                setUsersData(data);
            })
            .catch(error => console.log(error));
    },[])

    useEffect(() => {
        createMonthInfo();
    }, [usersData])


    return (
        <div className='container'>
            {
                isFetching ? <span className='container__loading'>Loading...</span> :
                <Table data={usersData} monthsInfo={monthsInfo}/>
            }
        </div>
    )

}

export default UserList;