interface Months {
    name: string;
    count: number;
}

export const months: Months[] = [{
        name: 'January',
        count: 1
    },{
        name: 'February',
        count: 2
    },{
        name: 'March',
        count: 3
    },{
        name: 'April',
        count: 4
    },{
        name: 'May',
        count: 5
    },{
        name: 'Jun',
        count: 6
    },{
        name: 'July',
        count: 7
    },{
        name: 'August',
        count: 8
    },{
        name: 'September',
        count: 9
    },{
        name: 'October',
        count: 11
    },{
        name: 'November',
        count: 11
    },{
        name: 'December',
        count: 12
    },
];

export const colorChecker = (count: number): string => {
    const between = (x: number, min: number, max: number) => {
        return x >= min && x <= max;
    }
    if(between(count, 0, 2)) {
        return 'grey'
    }
    if(between(count, 3, 6)) {
        return 'blue'
    }
    if(between(count, 7, 10)) {
        return 'green'
    }
    return 'red';
}