import React from "react";
import {MonthsInfo} from "../userList/UserList";
import './MonthsList.styles.scss'

interface MonthsListProps {
    months: MonthsInfo[];
    setFilter: (monthCount: number) => void;
    clearFilter: () => void;
}

const MonthsList: React.FC<MonthsListProps> = ({months, setFilter, clearFilter}) => {

    const onHandleMouseEnter = (monthCount: number) => {
        setFilter(monthCount - 1);
    }

    const onHandleMouseLeave = () => {
        clearFilter()
    }

    const renderMonths = (month: MonthsInfo) => (
        <div
            style={{backgroundColor: month.color}}
            className='months-container__item'
            onMouseEnter={() => onHandleMouseEnter(month.count)}
            onMouseLeave={onHandleMouseLeave}
            key={month.name}
        >
            <span className='months-container__item__title'>{month.name}</span>
        </div>
    )

    return (
        <div className='months-container'>
            {months.map(item => renderMonths(item))}
        </div>
    )

}

export default MonthsList;