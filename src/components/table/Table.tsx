import React, {useEffect, useState} from "react";
import TableUI from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import {User} from "../../entities/User";
import useStyles from "./Table.styles";
import MonthsList from "../monthsList/MonthsList";
import {MonthsInfo} from "../userList/UserList";
import {format} from 'date-fns';

interface TableProps {
    data: User[];
    monthsInfo: MonthsInfo[];
}

const Table: React.FC<TableProps> = ({data, monthsInfo}) => {


    useEffect(() => {
        setTableData(data)
    }, [data])

    const [tableData, setTableData] = useState<User[]>([]);

    const handleFilterTableData = (monthCount: number) => {
        setTableData(data.filter(item => new Date(item.dob).getMonth() === monthCount));
    }

    const handleClearFilter = () => {
        setTableData(data);
    }

    const classes = useStyles();

    const renderTableRow = (item: User) => (
        <TableRow key={item.id}>
            <TableCell align='center'>
                {item.id}
            </TableCell>
            <TableCell align='center'>{item.firstName}</TableCell>
            <TableCell align='center'>{item.lastName}</TableCell>
            <TableCell align='center'>{format(new Date(item.dob), 'dd.MM.yyyy')}</TableCell>
        </TableRow>
    )

    return (
        <>
            <TableContainer className={classes.tableBody} component={Paper}>
                <TableUI aria-label="simple table" stickyHeader>
                    <TableHead>
                        <TableRow>
                            <TableCell align="center">id</TableCell>
                            <TableCell align='center'>First Name</TableCell>
                            <TableCell align='center'>Last Name</TableCell>
                            <TableCell align='center'>Date of Birthday</TableCell>
                        </TableRow>
                    </TableHead>
                        <TableBody>
                            {tableData.map((item) => renderTableRow(item))}
                        </TableBody>
                </TableUI>
            </TableContainer>
            <MonthsList months={monthsInfo} setFilter={handleFilterTableData} clearFilter={handleClearFilter}/>
        </>
    )
}

export default Table;