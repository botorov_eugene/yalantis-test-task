import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
    mainContainer: {
        display: "flex",
        alignItems: "flex-start",
        background: 'red'
    },
    tableBody: {
        maxHeight: '100VH',
        minWidth: '75%',
        maxWidth: '80vh'
    }
})

export default useStyles;